package com.springMVC.controller;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

@RequestMapping(value="view")
@Controller(value = "springMVCController")
public class SpringMVCController {
	private static final Logger logger = LoggerFactory.getLogger(SpringMVCController.class);
		
	@RenderMapping
	public String renderExchangeUserProfile(RenderRequest renderRequest, RenderResponse renderResponse) {
		logger.trace("Entering renderExchangeUserProfile render method...");
		
        return "view";
	}
	
	@ResourceMapping(value = "")
	public void renderExchangeUserEdit(ResourceRequest resourceRequest, ResourceResponse resourceResponse) {
		logger.trace("Entering renderExchangeUserEdit render resource method...");
	}
	
}
